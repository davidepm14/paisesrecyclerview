package com.example.proyectobanderas;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

public class AdapterBanderas extends RecyclerView.Adapter<AdapterBanderas.MyViewHolder>  /*implements View.OnClickListener*/ {
    List<Paises> paises;
    //Context context;
    private View.OnClickListener listener;
    public AdapterBanderas(List<Paises> paises) {
        this.paises = paises;
    }



    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nombre,desc,cap;
        ImageView img;
        WebView wv;
        Button btn;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            //context=itemView.getContext();
            nombre = itemView.findViewById(R.id.idNombre);
            desc = itemView.findViewById(R.id.idInfo);
            img = itemView.findViewById(R.id.idImagen);
            cap = itemView.findViewById(R.id.idCapital);
            wv = itemView.findViewById(R.id.webview);
            btn = itemView.findViewById(R.id.boton);
        }
    }
    public void setOnClickListener(View.OnClickListener listener){
        this.listener=listener;
    }

    @NonNull
    @Override
    public AdapterBanderas.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_items, null, false);
        //v.setOnClickListener(this);
        return new AdapterBanderas.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Paises p=paises.get(position);
        //holder.img.setImageResource(p.getFoto());
        Glide.with(holder.itemView.getContext()).load(p.getFoto()).apply(new RequestOptions().override(180, 150)).into(holder.img);
        holder.desc.setText(p.getInfo());
        holder.nombre.setText(p.getName());
        holder.cap.setText(p.getCapital());
        holder.wv.getSettings().setJavaScriptEnabled(true);
        holder.wv.setWebViewClient(new WebViewClient());
        holder.wv.loadUrl(p.getUrl());
        //boton ver mapa envio de coordenadas y nombre del pais
        holder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent i=new Intent(view.getContext(),Mapa.class);
                    i.putExtra("latitud",p.getLatitud());
                    i.putExtra("longitud",p.getLongitud());
                    i.putExtra("pais",p.getName());
                    view.getContext().startActivity(i);
            }
        });

    }


    @Override
    public int getItemCount() {
        return paises.size();
    }


}
