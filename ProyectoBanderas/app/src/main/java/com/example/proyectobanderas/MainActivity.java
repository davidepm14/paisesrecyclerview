package com.example.proyectobanderas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.solver.widgets.Helper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.proyectobanderas.databinding.ActivityMainBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.common.collect.ImmutableList;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firestore.v1.ListDocumentsRequest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class
MainActivity extends AppCompatActivity {


    private int contador=1;
    private ActivityMainBinding binding;
    FirebaseFirestore db;
    //RecyclerView recyclerView;
    //ArrayList<Paises>paises;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        binding=ActivityMainBinding.inflate(getLayoutInflater());
        View view=binding.getRoot();
        setContentView(view);
         db= FirebaseFirestore.getInstance();

         //la construccion del RecyclerView se hace despues del metodo "traerFirebase"
        //binding.recId.setHasFixedSize(true);
        //binding.recId.setLayoutManager(new LinearLayoutManager(this));

        //llenar con archivo csv
        //List<Paises> paises = llenar();

        //AdapterBanderas mAdapter = new AdapterBanderas(paises);
        //binding.recId.setAdapter(mAdapter);

        //metodo traer los datos de firebase
        traerFirebase();

        //METODO PARA ENVIAR DATOS DE PAISES A CLOUD FIRESTORE
        //subirFireBase();
    }
    protected List<Paises> llenar(){
        List<Paises> paises=new ArrayList<>();

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(getAssets().open("lista.csv")));
            String line;
            Log.e("Reader Stuff",reader.readLine());

            while ((line = reader.readLine()) != null) {
                Log.e("code",line);
                String[] d = line.split(";");
                paises.add(new Paises(d[0],d[1],d[2],d[3],d[4],Double.parseDouble(d[5]),Double.parseDouble(d[6])));
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return paises;
    }
    public void subirFireBase(){
        List<Paises> paises= llenar();
        for(Paises pais:paises) {
            Map<String, Object> paisesMap = datos(pais);
            db.collection("Paises").add(paisesMap);
        }
    }
    public Map<String,Object> datos(Paises paises){
        Map<String,Object> pais=new HashMap<>();
        pais.put("id",contador++);
        pais.put("name",paises.getName());
        pais.put("info",paises.getInfo());
        pais.put("foto",paises.getFoto());
        pais.put("url",paises.getUrl());
        pais.put("capital",paises.getCapital());
        pais.put("latitud",paises.getLatitud());
        pais.put("longitud",paises.getLongitud());
        return pais;
    }

    public void traerFirebase(){
        db.collection("Paises").orderBy("id").limit(193).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){

                            List<Paises>paisesList=new ArrayList<>();

                            for(QueryDocumentSnapshot query:task.getResult()){

                                String name=query.getString("name");
                                String info=query.getString("info");
                                String foto=query.getString("foto");
                                String url=query.getString("url");
                                String capital=query.getString("capital");
                                double latitud=query.getDouble("latitud");
                                double longitud=query.getDouble("longitud");

                                Paises p=new Paises(name,info,foto,url,capital,latitud,longitud);

                                paisesList.add(p);
                            }
                            cargarRecycler(paisesList);
                        }

                    }
                });
    }
    private void cargarRecycler(List<Paises>paises){
        binding.recId.setHasFixedSize(true);
        binding.recId.setLayoutManager(new LinearLayoutManager(this));

        AdapterBanderas mAdapter = new AdapterBanderas(paises);
        binding.recId.setAdapter(mAdapter);
    }

}