# PaisesRecyclerView

## Componentes Utilizados
1. RecyclerView
2. Fragment
3. LinearLayout,ContraintLayout, CardView,
4. WebView

### Utilizacion de "ViewBinding" para mejor referencia de los ID
  - android.buildFeatures.viewBinding = true

## Dependencias/Implementaciones
  - implementation 'com.github.bumptech.glide:glide:4.11.0'
  - annotationProcessor 'com.github.bumptech.glide:compiler:4.11.0'
  - implementation 'androidx.recyclerview:recyclerview:1.1.0'
  - implementation 'androidx.cardview:cardview:1.0.0'
## Realizacion de Mapa(Google Maps)
implementacion de Actividad para Google Maps
  - implementation 'com.google.android.gms:play-services-maps:17.0.0'


### Codigo Creacion Mapa:


`LatLng sydney = new LatLng(-34, 151);`  
`LatLng sydney = new LatLng(latitud, longitud);`  
`mMap.addMarker(new MarkerOptions().position(sydney).title("title"));`  
`mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));`

## Implementacion de imagen y redimensionamiento con glide  
` @Override`  
` public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {`  
`     Glide.with(holder.itemView.getContext()).load(p.getFoto()).apply(new RequestOptions().override(360, 330)).into(holder.img);`  
` }`

## Evento del Boton
`//boton ver mapa envio de coordenadas y nombre del pais`  
`holder.btn.setOnClickListener(new View.OnClickListener() {`  
`   @Override`  
`   public void onClick(View view) {`  
`     Intent i=new Intent(view.getContext(),Mapa.class);`  
`     i.putExtra("latitud",p.getLatitud());`  
`     i.putExtra("longitud",p.getLongitud());`  
`     i.putExtra("pais",p.getName());`  
`     view.getContext().startActivity(i);`  
`   }`  
`});`  

## Capturas de la APP
<p align="center">
  <img src="https://gitlab.com/davidepm14/paisesrecyclerview/-/raw/master/images/foto.PNG" alt="Size Limit CLI" width="200">
</p>
<p align="center">
  <img src="https://gitlab.com/davidepm14/paisesrecyclerview/-/raw/master/images/foto2.PNG" alt="Size Limit CLI" width="200">
</p>








